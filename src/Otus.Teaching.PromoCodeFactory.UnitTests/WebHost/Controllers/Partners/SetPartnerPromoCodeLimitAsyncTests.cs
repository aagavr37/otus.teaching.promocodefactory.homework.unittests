﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly IFixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(NullPartner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = BasePartner;
            partner.IsActive = false;
            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SetLimit_ActiveLimitIsDisabled()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = BasePartner;
            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,
                new SetPartnerPromoCodeLimitRequest() { Limit = 1 });

            // Assert
            partner.NumberIssuedPromoCodes.Should().Equals(0);
            partner.PartnerLimits.Any(_ => _.CancelDate != null && _.CancelDate < DateTime.Now).Should().BeTrue();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SetNegativeLimit_ReturnsBadReuest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = BasePartner;
            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,
                new SetPartnerPromoCodeLimitRequest() { Limit = -1 });

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SetNewLimit_ContainDataInDB()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = BasePartner;
            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var limit = new SetPartnerPromoCodeLimitRequest() { Limit = 11, EndDate = DateTime.Now };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // assert
            var found = await _partnersRepositoryMock.Object.GetByIdAsync(partner.PartnerLimits.Last().Id);
            found.Should().NotBeNull();
        }


        private Partner NullPartner => null;

        private Partner BasePartner
            => new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
    }
}